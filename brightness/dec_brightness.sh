#!/bin/bash

# Takes an input of a number, to subtract from the current brightness.

DIR=/sys/class/backlight/intel_backlight
LVL=$1

MIN=100
CURR=$(head -n 1 "$DIR/brightness")

NEW=$(($CURR-$LVL))

if [ "$NEW" -lt "$MIN" ]; then
  tee "$DIR/brightness" <<< $MIN > /dev/null
  exit
else
  tee "$DIR/brightness" <<< $NEW > /dev/null
fi
