#!/bin/bash

# My laptop's brightness control doesn't automatically work
# by itself, and xbacklight is looking in the wrong directory
# to change my backlight settings, so I quickly wrote up these
# scripts to bindsym them to my brightness control keys. Simple
# and clean.

# Make sure that whatever you're using to call them has sufficient
# perimissions.

# Takes an input of a number, to add to the current brightness.

DIR=/sys/class/backlight/intel_backlight
LVL=$1

MAX=$(head -n 1 "$DIR/max_brightness")
CURR=$(head -n 1 "$DIR/brightness")

NEW=$(($LVL+$CURR))

if [ "$NEW" -gt "$MAX" ]; then
	tee "$DIR/brightness" <<< $NEW > /dev/null
	exit
else
	tee "$DIR/brightness" <<< $NEW > /dev/null
fi
